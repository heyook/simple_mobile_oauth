require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "creates an identity" do
    user = User.create
    identity = user.identities.create uid: 'abc', provider: 'provider'
    assert identity.persisted?
  end
end
