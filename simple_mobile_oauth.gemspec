$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "simple_mobile_oauth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "simple_mobile_oauth"
  s.version     = SimpleMobileOauth::VERSION
  s.authors     = ["Lin He"]
  s.email       = ["he9lin@gmail.com"]
  s.homepage    = "https://bitbucket.org/he9lin/simple_mobile_oauth"
  s.summary     = "Very Heyook projects opinionated mobile OAuth solution."
  s.description = "Very Heyook projects opinionated mobile OAuth solution."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.6"

  s.add_development_dependency "sqlite3"
end
