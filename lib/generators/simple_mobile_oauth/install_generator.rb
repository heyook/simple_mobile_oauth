require 'rails/generators/base'

module SimpleMobileOauth
  module Generators
    class InstallGenerator < Rails::Generators::Base
      include Rails::Generators::Migration

      source_root File.expand_path("../../templates", __FILE__)

      desc "Generates a SimpleMobileOauth migrations for identity model"

      def self.orm
        Rails::Generators.options[:rails][:orm]
      end

      def self.orm_has_migration?
        [:active_record].include? orm
      end

      def self.next_migration_number(path)
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      end

      def create_migration_file
        if self.class.orm_has_migration?
          migration_template 'migration.rb', 'db/migrate/simple_mobile_oauth_migration.rb'
        end
      end

      # def copy_initializer
      #   template "simple_mobile_oauth.rb", "config/initializers/simple_mobile_oauth.rb"
      # end

      def copy_identity_model
        template "identity.rb", "app/models/identity.rb"
      end
    end
  end
end
