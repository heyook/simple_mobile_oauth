require 'rails/generators/named_base'

module SimpleMobileOauth
  module Generators
    class SimpleMobileOauthGenerator < Rails::Generators::NamedBase
      include Rails::Generators::ResourceHelpers

      namespace 'simple_mobile_oauth'
      source_root File.expand_path("../templates", __FILE__)

      desc 'Insert include for given NAME model'
      hook_for :orm
    end
  end
end
