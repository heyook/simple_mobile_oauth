class Identity < ActiveRecord::Base
  validates :uid, :provider, presence: true
  validates_uniqueness_of :uid, scope: [:provider, :identifiable_type]

  belongs_to :identifiable, polymorphic: true

  serialize :info, JSON

  def username
    info["nickname"]
  end

  def avatar_url
    info["image"]
  end
end
