module SimpleMobileOauth
  module Identifiable
    extend ActiveSupport::Concern

    included do
      has_many :identities, as: :identifiable
    end
  end
end
